User's Guide
============

This guide contains detailed documentation about each component of Ferris.

.. toctree::
  :maxdepth: 2

  folder_structure
  mvc
  models
  controllers
  authorization_chains
  request_parsers
  response_handlers
  routing
  static_files
  views
  templates
  scaffolding
  components
  behaviors
  forms
  messages
  events
  plugins
  oauth2
  google_api_helper
  settings
  caching
  search
  mail
  uploads_and_downloads
  testing
  packages
